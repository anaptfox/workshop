var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});

  if(req.url.toLowerCase() === '/hello' && req.method === 'GET') {
    res.end('World!');
  } else if(req.method === 'POST') {

    // initialize a data buffer
    var data = '';

    // listen for data chunks and add each chunk to the data buffer
    req.on('data', function(chunk) {
      data += chunk;
    });

    // when the data is finished being sent, simple return the buffer back
    // as the response
    req.on('end', function() {
      res.end(data);
    });
  } else {
    res.end('Try GET /hello or any POST.');
  }
}).listen(1337);
console.log('Server running at http://localhost:1337/');
