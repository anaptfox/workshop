var express = require('express');
var router = express.Router();
var moment = require('moment');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/date', function(req, res) {
  res.end(moment().format('MM, DD ,YY'));
  res.render('index', { date: moment().format('MM, DD ,YY') });
});

router.get('/hi', function(req, res) {
  res.render('index', { title: 'Hi' });
});

module.exports = router;
