var http = require('http'),
    fs = require('fs');

// stream a file directly to the response
// this will buffer some of the file, but not all of it at once
var server = http.createServer(function (req, res) {
  fs.createReadStream('./users.csv').pipe(res);
});

server.listen(1337);
console.log('Server running at http://localhost:1337/');
