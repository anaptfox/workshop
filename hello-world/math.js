// Math 

module.exports.add = function(a, b){
  return a + b;
};

module.exports.substract = function(a, b){
  return a - b;
};

module.exports.multiply = function(a, b){
  return a * b;
};

module.exports.divide = function(a, b){
  return a / b;
};

module.exports.modulo = function(a, b){
  return a % b;
};