var http = require('http'); // load the core HTTP module

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});

  // if the route request is /hello via a GET request
  // send a "World!" response
  // otherwise send some instructions
  if(req.url.toLowerCase() === '/hello' && req.method === 'GET') {
    res.end('World!');
  } else {
    res.end('Try GET /hello.');
  }
}).listen(1337);
console.log('Server running at http://localhost:1337/');
