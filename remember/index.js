var fs = require('fs');

// Check for third argument
if(!process.argv[2]){
  console.log('Please include an argument.');
  process.exit();
}

//Check that the folder/file exists
if(!fs.existsSync(process.argv[2])){
  console.log('Please pass in file or folder.');
  process.exit();
}

// Get info on file or folder
var item = fs.lstatSync(process.argv[2]);

// Check to see if it is a file or folder
if(item.isDirectory()){
  console.log(process.argv[2] + ' is a directory');
}else{
  console.log(process.argv[2] + ' is not a directory');
}

process.exit();