var express = require('express');
var ItemController = require('../controllers/item');
var router = express.Router();

var data = {};

// add CRUD routes, using the router to make things a bit easier
router.get('/:id', function(req, res, next) { // get a single item
  var itemController = new ItemController();
  itemController.getOne(req.params.id, function(err, result){
    res.send(result);
  });
  //res.send(data[req.params.id] || null);
});

router.post('/', function(req, res, next) { // add an item to the collection
  var itemController = new ItemController();
  itemController.create(req.body, function(err, result){
    res.send(result);
  });
});

router.put('/:id', function(req, res, next) { // update an item
  if (req.body && typeof req.body === 'object' &&
    typeof data[req.params.id] === 'object') {
    Object.keys(req.body).forEach(function(key) {
      data[req.params.id][key] = req.body[key];
    });
  }
  res.send(data[req.params.id]);
});

router.delete('/:id', function(req, res, next) { //remove an item
  if (typeof data[req.params.id] === 'object') {
    delete data[req.params.id];
  }
  res.send(true);
});


module.exports = router;