// Math 

var _ = require('lodash');

module.exports.add = function(a, b, callback){
  // something is going on 
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a + b;
  callback(null, result);
};

module.exports.substract = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a - b
  callback(null, result);
};

module.exports.multiply = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a * b
  callback(null, result);
};

module.exports.divide = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a / b
  callback(null, result);
};

module.exports.modulo = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a % b;
  callback(null, result);
};