var http = require('http'),
    fs = require('fs');

var server = http.createServer(function (req, res) {

  // load a file, then send it in the response
  // this will buffer the entire file into memory before
  // sending it
  fs.readFile('./users.csv', function (err, data) {
    res.end(data);
  });
});

server.listen(1337);
console.log('Server running at http://localhost:1337/');
