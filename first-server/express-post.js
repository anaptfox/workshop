var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'); // load the body parser

// set up the body parser
// this will parse incoming request data into a consumable format
// this also makes it available inside requests
app.use(bodyParser());

app.get('/hello', function(req, res){
  res.send('World!');
});

// add a POST route that just returns the request body passed in
app.post('*', function(req, res) {
  res.send(req.body);
});

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});
