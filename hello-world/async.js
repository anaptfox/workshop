var fs = require('fs');

for(var i = 0; i < 10; i++) {
  // using the asynchronous version will cause each save to run in parallel
  // this means they will finish out of order and randomly, but much faster
  fs.writeFile('file' + i + '.txt', 'Hello World!', function(err) {
    console.log('File ' + this + ' saved.');
  }.bind(i));
  // binding to "i" allows us to access the current iteration
  // later on, even after the loop has finished
}
