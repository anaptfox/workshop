var fs = require('fs');

var guess = process.argv[2];

if(!guess){
  console.log('Please include a third argument');
  process.exit();
}

if(!fs.existsSync(guess)){
  console.log('That is not a real path');
  process.exit();
}

fs.lstat(guess, function(err, data){
  if(err){
    console.log('An error as occurred ' + err);
    process.exit();
  }
  
  if(data.isDirectory()){
    console.log('It is a directory');
  }else{
    console.log('It is not a directory');
  }

  console.log('The size is ' + data.size);
});