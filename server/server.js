var http = require('http');
var request = require('request');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  request.get('http://google.com', function (error, response, body) {
    console.log(body);
    res.end(body);
  })
  
}).listen(1337, '127.0.0.1');

console.log('Server running at http://127.0.0.1:1337/');