var express = require('express'),
    app = express();              // create an express app

// add a GET route to the server
app.get('/', function(req, res){
  res.send('Hello World!');
});

app.get('/hello', function(req, res){
  res.send('World!');
});

// start the server
var server = app.listen(1337, function() {
  console.log('Listening on port %d', server.address().port);
});
