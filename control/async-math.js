
var async = require('async');
var math = require('./math.js');

var arrayOfNumbers = [1,1,1,1,1,1,1,1,1,1,1,1,1,1];

// async.waterfall([
//     function(callback){
//         math.add(1, 1, callback)    
//     },

//     function(resultOfAdd, callback){
//       math.substract(resultOfAdd, 1, callback) 
//     }
// ], function (err, result) {
//     console.log(result);
//    // result now equals 'done'    
// });

var newArrayOfNumbers = [];

async.each(arrayOfNumbers, function(item, callback){
  math.add(item, 1, function(err, result){
    newArrayOfNumbers.push(result);
    callback();
  });
}, function(){
  console.log('Before: ' + arrayOfNumbers);
  console.log('After: ' + newArrayOfNumbers);
})
