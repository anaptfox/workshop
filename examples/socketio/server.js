var fs = require('fs'),
    // create a simple server that serves up the HTML file
    server = require('http').createServer(function(req, res) {
      fs.createReadStream('./index.html').pipe(res);
    }),
    io = require('socket.io')(server); // create a socket.io server

server.listen(1337);
console.log('Server running at http://localhost:1337/');

// listen for a successful connections
io.on('connection', function (socket) {

  // send a message every 2 seconds
  setInterval(function() {
    socket.emit('news', {
      id: Math.floor(Math.random() * 100000),
      time: Math.floor(Date.now() / 1000)
    });
  }, 2000);

  // listen for an event from the client
  socket.on('my other event', function (data) {
    console.log(data);
  });
});
