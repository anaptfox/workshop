var fs = require('fs');

for(var i = 0; i < 10; i++) {
  // using the synchronous version will block the process until it completes
  // this will cause all the files to finish in order
  fs.writeFileSync('file' + i + '.txt', 'Hello World!');
  console.log('File ' + i + ' saved.');
}
