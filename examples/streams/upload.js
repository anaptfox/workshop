var formidable = require('formidable'), // multi-part form parser
    http = require('http'),
    uploadPath = './uploads';

var server = http.createServer(function(req, res) {

  // if posted to the /upload route, parse the form
  if (req.url == '/upload' && req.method.toLowerCase() == 'post') {

    var form = new formidable.IncomingForm({uploadDir:uploadPath});
    form.parse(req, function(err, fields, files) {

      // respond with information on the form parts that was processed
      res.writeHead(200, {'content-type': 'text/plain'});
      res.write('received upload:\n\n');
      res.end(JSON.stringify({fields:fields,files:files}, null, 2));
    });

    return;
  }

  // for any other request, send an upload form
  res.writeHead(200, {'content-type': 'text/html'});
  res.end(
    '<form action="/upload" enctype="multipart/form-data" method="post">'+
    '<input type="file" name="upload" multiple="multiple"><br>'+
    '<input type="submit" value="Upload">'+
    '</form>'
  );
});

server.listen(1337);
console.log('Server running at http://localhost:1337/');
