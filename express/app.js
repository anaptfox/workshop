var express = require('express');
var debug = require('debug');
var path = require("path");

var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/expressApp");

require('./models/item');

var routes = require('./routes/index');
var users = require('./routes/users');
var item = require('./routes/item');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    // body...
    next();
});

app.use('/', routes);

app.use('/users', users);

app.use('/item', item);

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});

