// Math 

var _ = require('lodash');
var Q = require('q');

module.exports.add = function(a, b){
  var deferred = Q.defer();
  // something is going on 
  if(!_.isNumber(a) || !_.isNumber(b)){
    deferred.reject('Please provide two numbers');
  }
  var result = a + b;
  deferred.resolve(result);
  return deferred.promise;
};

module.exports.substract = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a - b
  callback(null, result);
};

module.exports.multiply = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a * b
  callback(null, result);
};

module.exports.divide = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a / b
  callback(null, result);
};

module.exports.modulo = function(a, b, callback){
  if(!_.isNumber(a) || !_.isNumber(b)){
    return callback('Please provide two numbers', null);
  }
  var result = a % b;
  callback(null, result);
};