var fs = require('fs');

// stream a file directly to the response
// this will buffer some of the file, but not all of it at once
fs.createReadStream('./users.csv').pipe(process.stdout);

