var express = require('express'),
  app = express(),
  bodyParser = require('body-parser'), // load the body parser
  data = {},
  router = express.Router(); // create a router object to use later

app.use(bodyParser.json()); // set up the body parser in express

// add CRUD routes, using the router to make things a bit easier
router.route('/item/:id')
  .get(function(req, res, next) { // get a single item
    res.send(data[req.params.id] || null);
  })
  .post(function(req, res, next) { // add an item to the collection
    req.body.id = req.params.id;
    data[req.params.id] = req.body;
    res.send(req.body);
  })
  .put(function(req, res, next) { // update an item
    if (req.body && typeof req.body === 'object' &&
      typeof data[req.params.id] === 'object') {
      Object.keys(req.body).forEach(function(key) {
        data[req.params.id][key] = req.body[key];
      });
    }

    res.send(data[req.params.id]);
  })
  .delete(function(req, res, next) { //remove an item
    if (typeof data[req.params.id] === 'object') {
      delete data[req.params.id];
    }

    res.send(true);
  });

// add the router to the express app
// this adds the routes
app.use(router);

// add a route that gets all the items
app.get('/items', function(req, res) {
  res.send(data);
});

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});