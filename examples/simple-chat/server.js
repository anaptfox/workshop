var express = require('express'),
    app = express(),
    winston = require('winston'),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

winston.add(winston.transports.File, { filename: 'chat.log' });

var users = {};

app.use('/', express.static(__dirname + '/public'));

io.sockets.on('connection', function (socket) {
  
  winston.info("A user has connected " + socket.id);
  users[socket.id] = {};

  socket.on('msg', function (data) {
    users[socket.id].name = data.name;
    winston.info(JSON.stringify(data));
    io.sockets.emit('new', data);
  });

  socket.on('disconnect', function(){
    winston.info("A user has disconnected: " + users[socket.id].name);
  });


});

server.listen(process.env.PORT || 1337);
console.log('Server running at http://localhost:1337/');
