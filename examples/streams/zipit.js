var http = require('http'),
    archiver = require('archiver'), // stream zipper
    fs = require('fs');

var server = http.createServer(function (req, res) {

  // set response headers for sending a zip stream
  res.writeHead(200, {
    'Content-Type': 'application/zip',
    'Content-disposition': 'attachment; filename=users.zip'
  });

  // create the zip object and set up the pipe
  var zip = archiver('zip');
  zip.pipe(res);

  // add the files to zip and start the stream
  zip.append(fs.createReadStream('./users.csv'), {name:'data/users.csv'})
    .finalize();
});

server.listen(1337);
console.log('Server running at http://localhost:1337/');
