var debug = require('debug')('controller:item');
var mongoose = require('mongoose'),
  Item = mongoose.model('Item');

var ItemController = function() {
  debug('init');
};


ItemController.prototype.create = function(item, callback) {

  var item = new Item(item);

  item.save(function(err, result) {
    if (err) {
      console.log(err)
      return callback(err);
    }
    return callback(null, result)
  })

};

ItemController.prototype.getOne = function(id, callback) {
  Item.findById(id, function(err, item) {
    if (err) {
      return callback(err);
    } else {
      if (!item) {
        return callback('Project not found');
      }
    }
    debug('Item %s found', id);
    callback(null, item);
  });
};

ItemController.prototype.getAll = function(callback) {
   Item.find(function(err, items) {
    if (err) {
      return callback(err);
    } 
    debug('Items found', id);
    callback(null, items);
  });
};

ItemController.prototype.update = function(id, item, callback) {
  Item.findByIdAndUpdate(id, item, function(err, item) {
     if (err) {
       return callback(err);
     } else {
       if (!item) {
         return callback('Item not found');
       }
     }
     debug('Item %s updated', id);
     callback(null, item);
   });

};

ItemController.prototype.delete = function(id, callback) {
  Item.findOneAndRemove(id, function(err) {
    if (err) {
      return callback(err);
    }
    debug('Deleting item %s', id);
    callback();
  });
};

module.exports = ItemController;