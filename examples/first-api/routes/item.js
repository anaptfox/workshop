var express = require('express'),
    items = {};

// export a function that adds the item routes to the express app
module.exports = function(app) {
  var itemRouter = express.Router();

  // add validation to the id parameter
  // if it is not a number, return an error
  itemRouter.param('id', function(req, res, next, id) {
    if(/^\d+$/.test(id)) {
      next();
    } else {
      next(new Error('Item Id must be a number.'));
    }
  });

  itemRouter.route('/item/:id')
    .get(function(req, res, next){
      res.send(items[req.params.id] || null);
    })
    .post(function(req, res, next) {
      req.body.id = req.params.id;
      items[req.params.id] = req.body;
      res.send(req.body);
    })
    .put(function(req, res, next) {
      if(req.body && typeof req.body === 'object' && typeof items[req.params.id] === 'object') {
        Object.keys(req.body).forEach(function(key) {
          items[req.params.id][key] = req.body[key];
        });
      }

      res.send(items[req.params.id]);
    })
    .delete(function(req, res, next) {
      if(typeof items[req.params.id] === 'object') {
        delete items[req.params.id];
      }

      res.send(true);
    });

  app.get('/items', function(req, res) {
    res.send(items);
  });

  app.use(itemRouter);
};
