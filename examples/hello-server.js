// load the core HTTP module
var http = require('http');

// create a server that always responds with "Hello World"
http.createServer(function (req, res) {

  // write the required response headers
  res.writeHead(200, {'Content-Type': 'text/plain'});

  // send the response
  res.end('Hello World\n');
}).listen(1337);

console.log('Server running at http://localhost:1337/');
