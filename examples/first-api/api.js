var express = require('express'),
    app = express(),
    bodyParser = require('body-parser');

app.use(bodyParser.json());

// load the item routes
require('./routes/item')(app);

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});
