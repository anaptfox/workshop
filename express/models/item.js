/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Project Schema
 */

var ItemSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  active: {
    type: Boolean,
    default: false,
    required: true
  },
  created: {
    type: Date,
    default: new Date()
  },
  image: {
    type: String
  }
});


mongoose.model('Item', ItemSchema);