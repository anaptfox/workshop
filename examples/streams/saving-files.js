var request = require('request'),
    fs = require('fs');

// get an image from a website and pipe it to a file
request('http://actionholder.com/i/250')
  .pipe(fs.createWriteStream('action.png'));
